from typing import Dict, List


def parse_hours_data(data: str) -> Dict[str, List[int]]:
    """Разбирает входные данные и возвращает словарь с информацией о количестве часов работы для каждого работника.

    Args:
        data (str): Входные данные с именами работников и количеством часов работы.

    Returns:
        Dict[str, List[int]]: Словарь, где ключи - имена работников (строки),
        а значения - списки количества часов работы для каждого работника.
    """

    hours_dict = {}
    entries = data.split('\n')
    for entry in entries:
        if not entry.strip():  # Пропускаем пустые строки
            continue
        last_space_index = entry.rfind(' ')
        name, hours_str = entry[:last_space_index], entry[last_space_index + 1:]
        name = name.strip()
        hours = int(hours_str)

        if name in hours_dict:
            hours_dict[name].append(hours)
        else:
            hours_dict[name] = [hours]

    return hours_dict


def print_statistics(hours_dict: Dict[str, List[int]]) -> None:
    """Выводит статистику для каждого работника, включая количество часов работы и общую сумму.

    Args:
        hours_dict (Dict[str, List[int]]): Словарь с информацией о количестве часов работы для каждого работника.
    """

    for name, hours_list in hours_dict.items():
        hours_str = ', '.join(map(str, hours_list))
        total_hours = sum(hours_list)
        print(f"{name}: {hours_str}; sum: {total_hours}")


if __name__ == "__main__":
    input_data = \
    """
    Андрей 9
    Василий 11
    Роман 7
    X Æ A-12 45
    Иван Петров 3
    Андрей 6
    Роман 11
    """

    hours_data = parse_hours_data(input_data)
    print_statistics(hours_data)
